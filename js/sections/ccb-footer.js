let target = document.querySelector('.special');

document.querySelector('.option-1').addEventListener('click', function () {
    if (!this.className.includes('active')) { //ver se esta opção já está selecionada
        activeOption(1); //se não está selecionado, chamo a função com o index da opção
    }
})
document.querySelector('.option-2').addEventListener('click', function () {
    if (!this.className.includes('active')) {
        activeOption(2);
    }
})
document.querySelector('.option-3').addEventListener('click', function () {
    if (!this.className.includes('active')) {
        activeOption(3);
    }
})
document.querySelector('.option-4').addEventListener('click', function () {
    if (!this.className.includes('active')) {
        activeOption(4);
    }
})

function removeScaleIn() {
    // let id = document.querySelector('.option-container.active').getAttribute('ccb-section');     //saber qual o id anterior
    // document.getElementById(id).classList.remove('scale-in');
    let prev = document.querySelector('.ccb-main.scale-in');
    prev.classList.remove('scale-in');
    setTimeout(function () {
        prev.classList.add('hide');
    }, 250)
}

function inicioComponent() {
    let opt = document.querySelector('.option-1');                          //guardar a opção numa variavel
    opt.classList.add('active');                                            //adicionar classe active na opção(variavel)
    opt.children[0].classList.replace('bg-inicio', 'bg-inicio_hover');      //trocar a class cinzenta pela vermelha no icon
    target.classList.add('first');                                           //adicionar ao target a nova localização
    document.querySelector('#inicio').classList.remove('hide');
    setTimeout(function () {
        document.querySelector('#inicio').classList.add('scale-in');
        window.scrollTo(0, 0);
    }, 250)
}

function calendarComponent() {
    let opt = document.querySelector('.option-3');
    opt.classList.add('active');
    opt.children[0].classList.replace('bg-calendar', 'bg-calendar_hover');
    target.classList.add('third')
    document.querySelector('#calendar').classList.remove('hide');
    setTimeout(function () {
        document.querySelector('#calendar').classList.add('scale-in');
        window.scrollTo(0, 0);
    }, 250)
}

function activeOption(number) {
    removeScaleIn();
    target.classList.remove('first', 'second', 'third', 'fourth');              //remover a localização do target
    clearStyles();                                                              //chamar esta função para remover todos os estilos aplicados às opções (remover os vermelhos)
    if (number === 1) {
        inicioComponent();
    }
    else if (number === 2) {
        let opt = document.querySelector('.option-2');
        opt.classList.add('active');
        opt.children[0].classList.replace('bg-cartao', 'bg-cartao_hover');
        target.classList.add('second');
        document.querySelector('#cartao').classList.remove('hide');
        setTimeout(function () {
            document.querySelector('#cartao').classList.add('scale-in');
            window.scrollTo(0, 0);
        }, 250)
    }
    else if (number === 3) {
        calendarComponent();
    }
    else if (number === 4) {
        let opt = document.querySelector('.option-4');
        opt.classList.add('active');
        opt.children[0].classList.replace('bg-perfil', 'bg-perfil_hover');
        target.classList.add('fourth');
        document.querySelector('#perfil').classList.remove('hide');
        setTimeout(function () {
            document.querySelector('#perfil').classList.add('scale-in');
            window.scrollTo(0, 0);
            initPerfilComponent();
        }, 250)
    }
}

function clearStyles() {
    document.querySelectorAll('.option-container').forEach(function (it) {              //loop por todos os icones
        it.classList.remove('active');                                                  //remover a classe active de todos (sem saber qual estava activo)
        if (it.children[0].className.includes('_hover')) {                              //ver se existe uma class com _hover (para saber se está vermelha)
            let length = it.children[0].classList[1].length;                            //guardar numa variavel o tamanho dessa classe com _hover (ex. bg-cartao_hover tem 15 caracteres)
            let newClass = it.children[0].classList[1].substring(0, length - 6)         //criar uma nova classe, pegando na classe que tem o _hover e cortar essa classe do caracter 0 ao caracter 10
            it.children[0].classList.replace(it.children[0].classList[1], newClass)     //trocar as classes (ex. bg-cartao_hover pela bg-cartao)
        }
    })
}

function hideFooter(status) {
    let foo = document.querySelector('.ccb-footer');
    if (status) foo.classList.remove('scale-in');
    else foo.classList.add('scale-in');
}