function initializeNotifications() {
    document.querySelector('#notificacoes').classList.remove('hide');
    hideFooter(true);
    menuBoolean = false;
    setTimeout(function () {
        document.querySelector('#notificacoes').classList.add('scale-in');
        window.scrollTo(0, 0);
    }, 250)
}

function finalizeNotificationsComponent() {
    document.querySelector('#notificacoes').classList.remove('scale-in');
    hideFooter(false);
    menuBoolean = true;
    setTimeout(function () {
        document.querySelector('#notificacoes').classList.add('hide');
    }, 250)
}

document.querySelector('.goback-arrow-notification').onclick = function () {
    inicioComponent();
    setTimeout(function () {
        finalizeNotificationsComponent();
    }, 250)
}