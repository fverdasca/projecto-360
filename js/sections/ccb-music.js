function initializeMusicComponent() {
    menuBoolean = false;
    hideFooter(true);
    document.querySelector('#ccb-musica').classList.remove('hide');
    setTimeout(function () {
        document.querySelector('#ccb-musica').classList.add('scale-in');
        window.scrollTo(0, 0);
    }, 250)
    musicEvents();
}

function finalizeMusicComponent() {
    document.querySelector('#ccb-musica').classList.remove('scale-in');
    hideFooter(false);
    menuBoolean = true;
    setTimeout(function () {
        document.querySelector('#ccb-musica').classList.add('hide');
    }, 250)
}

function musicEvents() {
    document.querySelector('.goback-arrow').onclick = function () {
        inicioComponent();
        setTimeout(function () {
            finalizeMusicComponent();
        }, 250)
    }
    document.querySelectorAll('#ccb-musica .card-categoria').forEach(function (card) {
        card.onclick = function () {
            removeScaleIn();
            initializeEventoComponent();
        }
    })
    document.querySelectorAll('#filter-list-3 a').forEach(function (filt) {
        filt.onclick = function () {
            if (!filt.className.includes('ccb')) {
                document.querySelector('#filter-list-3 .ccb').classList.remove('ccb');
                filt.classList.add('ccb');
            }
        }
    })
    document.querySelectorAll('#ccb-musica .favorite-event').forEach(function (icon) {
        icon.onclick = function () {
            let iconItself = icon.children[0];
            if (iconItself.className.includes('bookmarked')) {
                iconItself.classList.remove('bookmarked');
                iconItself.setAttribute('src', './svg/icons/card-page/eventoguardado.svg');
            } else {
                iconItself.classList.add('bookmarked');
                iconItself.setAttribute('src', './svg/icons/card-page/bookmark_filled.svg');
                M.toast({
                    html: 'Evento guardado!'
                })
            }
        }
    })
}