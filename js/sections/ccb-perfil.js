let elemPosition;

function initPerfilComponent() {
    document.querySelector('#perfil').onscroll = function () {
        checkElementPosition();
    }
    setTimeout(function () {
        elemPosition = document.querySelector('.overlay-container').getBoundingClientRect();
    }, 500)
}

function checkElementPosition() {
    let newPosition = document.querySelector('.overlay-container').getBoundingClientRect();
    if (newPosition.top <= elemPosition.top - 2) hideFooter(true);
    else hideFooter(false);
}