let elemPositionAtEvento;
let targetComponent = '';

function initializeEventoComponent(target) {
    if (target) targetComponent = target;
    else targetComponent = '';
    document.querySelector('#evento').classList.remove('hide');
    hideFooter(true);
    menuBoolean = false;
    document.querySelector('#evento').onscroll = function () {
        checkEventoElementPosition();
    }
    document.querySelector('.evento-back').onclick = function () {
        finalizeEventoComponent();
        if (targetComponent === '') initializeMusicComponent();
        else if (targetComponent === 'inicio') inicioComponent();
        else if (targetComponent === 'calendar') calendarComponent();
    }
    setTimeout(function () {
        document.querySelector('#evento').classList.add('scale-in');
        window.scrollTo(0, 0);
        elemPositionAtEvento = document.querySelector('.evento-container').getBoundingClientRect();
    }, 250)
}

function checkEventoElementPosition() {
    let newPositionAtEvento = document.querySelector('.evento-container').getBoundingClientRect();
    if (newPositionAtEvento.top <= elemPositionAtEvento.top - 2) {
        document.querySelector('.evento-back').classList.add('hide');
        document.querySelector('.evento-back').classList.remove('scale-in');
    }
    else {
        document.querySelector('.evento-back').classList.remove('hide');
        setTimeout(function () {
            document.querySelector('.evento-back').classList.add('scale-in');
        }, 500)

    }
}

function finalizeEventoComponent() {
    menuBoolean = true;
    hideFooter(false);
    document.querySelector('#evento').classList.remove('scale-in');
    setTimeout(function () {
        document.querySelector('#evento').classList.add('hide');
    }, 250)
}