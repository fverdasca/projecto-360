document.querySelectorAll('#calendar .card-horizontal .card .bookmark-icon').forEach(function (icon) {
    icon.onclick = function () {
        if (icon.className.includes('bookmarked')) {
            icon.classList.remove('bookmarked');
            icon.innerHTML = 'bookmark_border';
        } else {
            icon.classList.add('bookmarked');
            icon.innerHTML = 'bookmark';
        }
    }
})

document.querySelectorAll('#filter-list-1 a').forEach(function (filt) {
    filt.onclick = function () {
        if (!filt.className.includes('ccb-red')) {
            document.querySelector('#filter-list-1 .ccb-red').classList.remove('ccb', 'ccb-red');
            filt.classList.add('ccb', 'ccb-red');
        }
    }
})

document.querySelectorAll('#filter-list-2 a').forEach(function (filt) {
    filt.onclick = function () {
        if (!filt.className.includes('ccb')) {
            document.querySelector('#filter-list-2 .ccb').classList.remove('ccb');
            filt.classList.add('ccb');
        }
    }
})

document.querySelectorAll('#calendar .favorite-event').forEach(function (icon) {
    icon.onclick = function () {
        let iconItself = icon.children[0];
        if (iconItself.className.includes('bookmarked')) {
            iconItself.classList.remove('bookmarked');
            iconItself.setAttribute('src', './svg/icons/card-page/eventoguardado.svg');
        } else {
            iconItself.classList.add('bookmarked');
            iconItself.setAttribute('src', './svg/icons/card-page/bookmark_filled.svg');
            M.toast({
                html: 'Evento guardado!'
            })
        }
    }
})

document.querySelectorAll('#calendar .card-categoria').forEach(function (card) {
    card.onclick = function () {
        removeScaleIn();
        initializeEventoComponent('calendar');
    }
})

document.querySelector('#close-calendar-modal').onclick = function () {
    document.querySelector('.calendar-modal').classList.remove('scale-in');
    setTimeout(function () {
        document.querySelector('.calendar-modal').classList.add('hide');
    }, 250)
}

document.querySelector('#calendar-icon').onclick = function () {
    document.querySelector('.calendar-modal').classList.remove('hide');
    setTimeout(function () {
        document.querySelector('.calendar-modal').classList.add('scale-in');
    }, 250)
}