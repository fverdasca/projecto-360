document.querySelectorAll('.categories a').forEach(function (link) {
    link.removeAttribute('target');
    link.onclick = function () {
        removeScaleIn();
        initializeMusicComponent();
    }
})

document.querySelector('.container-explorar .img-evento').onclick = function () {
    removeScaleIn();
    initializeEventoComponent('inicio');
}

document.querySelector('#notifications-icon').onclick = function () {
    removeScaleIn();
    initializeNotifications();
}